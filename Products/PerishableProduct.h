#pragma once
#include "Product.h"

class PerishableProduct : public Product
{
public:
	PerishableProduct(int32_t id, std::string name, float rawPrice, const std::string& expirationDate);

	// Inherited via Product
	int32_t GetVat() const override;
	float GetPrice() const override;
	
	// Getters
	const std::string& GetExpirationDate() const;

private:
	static const int32_t kVAT = 9;

private:
	std::string m_expirationDate;
};

