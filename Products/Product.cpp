#include "Product.h"

Product::Product(int32_t id, const std::string& name, float rawPrice) :
	m_id(id),
	m_name(name),
	m_rawPrice(rawPrice)
{
	// empty
}

int32_t Product::GetID() const
{
	return m_id;
}

const std::string& Product::GetName() const
{
	return m_name;
}

float Product::GetRawPrice() const
{
	return m_rawPrice;
}
