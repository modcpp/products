#include "PerishableProduct.h"
#include "NonperishableProduct.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <regex>

using namespace std;

bool isDate(const string& sDate)
{
	return regex_match(sDate, regex(R"(\d\d\d\d-\d\d-\d\d)"));
}

NonperishableProduct::Type toNonperishableProductType(const string& sType)
{
	if (sType == "PersonalHygiene")
		return NonperishableProduct::Type::PersonalHygiene;
	else if (sType == "SmallAppliences")
		return NonperishableProduct::Type::SmallAppliances;
	else if (sType == "Clothing")
		return NonperishableProduct::Type::Clothing;

	throw "Error: Could not convert from string to NonperishableProduct::Type!";
}

int main()
{
	vector<IPriceable*> priceables;

	int32_t id;
	string name;
	float rawPrice;
	int32_t vat;
	string dateOrType;

	for (ifstream in("product.prodb"); !in.eof();)
	{
		// read line
		in >> id >> name >> rawPrice >> vat >> dateOrType;
		
		// construct object based on date or type
		if (isDate(dateOrType))
			priceables.push_back(new PerishableProduct(id, name, rawPrice, dateOrType));
		else
			priceables.push_back(new NonperishableProduct(id, name, rawPrice, toNonperishableProductType(dateOrType)));
	}

	auto nameComparator = [](const IPriceable* left, IPriceable* right)
	{
		return dynamic_cast<const Product*>(left)->GetName() < dynamic_cast<const Product*>(right)->GetName();
	};
	auto priceComparator = [](const IPriceable* left, IPriceable* right) 
	{ 
		return left->GetPrice() < right->GetPrice();
	};

	//sort(priceables.begin(), priceables.end(), nameComparator);
	sort(priceables.begin(), priceables.end(), priceComparator);

	// print Nonperishable Products
	for (auto priceable : priceables)
	{
		auto perishableProduct = dynamic_cast<NonperishableProduct *>(priceable);
		if (perishableProduct != nullptr)
			cout << *perishableProduct << endl;
	}

	// clean up
	for (auto priceable : priceables)
		delete priceable;

	return 0;
}