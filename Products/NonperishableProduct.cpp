#include "NonperishableProduct.h"

NonperishableProduct::NonperishableProduct(int32_t id, const std::string& name, float rawPrice, Type type) :
	Product(id, name, rawPrice),
	m_type(type)
{
	// empty
}

int32_t NonperishableProduct::GetVat() const 
{
	return kVat;
}

float NonperishableProduct::GetPrice() const 
{
	return m_rawPrice + m_rawPrice * GetVat() / 100.0f;
}

std::ostream& operator<<(std::ostream& os, const NonperishableProduct& prod)
{
	os << prod.GetID() << " " << prod.GetName() << " " << prod.GetPrice();
	return os;
}

