#pragma once
#include "IPriceable.h"

#include <string>

class Product :	public IPriceable
{
public:
	Product(int32_t id, const std::string& name, float rawPrice);

	// Getters
	int32_t GetID() const;
	const std::string& GetName() const;
	float GetRawPrice() const;

protected:
	int32_t m_id;
	std::string m_name;
	float m_rawPrice;
};

