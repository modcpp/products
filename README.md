# Products Application

You have been appointed the manager of a new general store. As you know some programming, you will be creating a software that manages the inventory logic.

## Requirements

### Phase 1

1. Read the products from the input file [product.prodb](https://elearning.unitbv.ro/mod/resource/view.php?id=60047) into a vector. Use a single class to represent a product. Watch out for member variable types, memory is precious! No setters allowed, we want a product object to be immutable!
1. Print only the NonperishableProducts on the screen, with the price calculated based on VAT.
1. Provide functionality to sort by name or by price.

### Phase 2

1. Commit your source code to BitBucket
1. Implement the class hierarchy from the diagram.
![Alt text](ProductsHierarchy.png?raw=true "Products Hierarchy")
1. Make sure the functionality from Phase 1 still works!
1. Commit often:
    * After each class you finish
    * After you added working functionality
    * Before refactoring!

<!-- Before export: -->
<!-- update link to products.prodb when exporting -->
<!-- phase 2 can be omitted at export -->